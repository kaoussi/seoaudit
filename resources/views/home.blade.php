@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>    
</div>
<div id="seomtr-container"></div>
<script
    id="seomtr-sdk"
    data-seomtr-domain="https://seomator.com"
    data-partner-code="3c6df2"
    data-mode="takeover"
    data-report-mode="embed"
    data-locale="en"
    type="text/javascript"
    src="https://seomator.com/assets/audit-tools/js/sdk.js">
</script>
@endsection
